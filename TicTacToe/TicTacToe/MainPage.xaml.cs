﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TicTacToe
{
    public partial class MainPage : ContentPage
    {
        private Button[] buttons = new Button[9];
        private Partidas juego = new Partidas();

        /// <summary>
        /// Se inicializan los botones para el momento de su ejecucion
        /// </summary>
        public MainPage()
        {
            InitializeComponent();
            buttons[0] = button1;
            buttons[1] = button2;
            buttons[2] = button3;
            buttons[3] = button4;
            buttons[4] = button5;
            buttons[5] = button6;
            buttons[6] = button7;
            buttons[7] = button8;
            buttons[8] = button9;


        }

        /// <summary>
        /// Se hacen las verificaciones cada que se oprime el boton
        /// </summary>
        private void button_Clicked(object sender, EventArgs e)
        {
            juego.SetButton((Button)sender);
            if (juego.Ganador(buttons))
            {
                finjuego.IsVisible = true;
            }
        }

        /// <summary>
        /// Se reinicia el juego x medio de este boton
        /// </summary>
        private void otravez_Clicked(Object sender, EventArgs e)
        {
            juego.ReiniciarPartida(buttons);
            finjuego.IsVisible = false;
        }
    }
}
