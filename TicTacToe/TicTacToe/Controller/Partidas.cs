﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace TicTacToe
{
    class Partidas
    {
        private int i, playerTurn = 1;


        /// <summary>
        /// Se hace un listado de los vectores posibles para ganar una partida
        /// </summary>
        private int[,] posiblesGanadores = new int[,]
        {
            {0,1,2},
            {3,4,5},
            {6,7,8},
            {0,3,6},
            {1,4,7},
            {2,5,8},
            {0,4,8},
            {2,4,6}

        };
        
        /// <summary>
        /// Se hace la validacion completa de si el movimiento es exitoso para ganar o si continua el juego
        /// </summary>
        public bool Ganador(Button[] buttons)
        {
            bool finjuego = false;
            for (i = 0; i < 8; i++)
            {
                int a = posiblesGanadores[i, 0], b = posiblesGanadores[i, 1], c = posiblesGanadores[i, 2];
                Button b1 = buttons[a], b2 = buttons[b], b3 = buttons[c];


                if (b1.Text == "" || b2.Text == "" || b3.Text == "")
                    continue;

                if (b1.Text == b2.Text && b2.Text == b3.Text)
                {
                    b1.BackgroundColor = b2.BackgroundColor = b3.BackgroundColor = Color.Red;
                    finjuego = true;
                    break;
                }
            }

            bool Empate = true;
            // Se hace la validacion de si se acaba o no el juego
            if (!finjuego)

            {
                foreach (Button b in buttons)
                {
                    if (b.Text == "")
                    {
                        Empate = false;
                        break;
                    }
                }
                if (Empate)
                {
                    finjuego = true;
                }
            }
            return finjuego;
        }

        /// <summary>
        /// Se hace el cambio de turnos, primero X y despues O
        /// </summary>
        public void SetButton(Button b)
        {
            if (b.Text == "")
            {
                b.Text = playerTurn == 1 ? "x" : "O";
                playerTurn = playerTurn == 1 ? 2 : 1;
            }
        }

        /// <summary>
        /// Se reinicia la partida actual
        /// </summary>
        public void ReiniciarPartida(Button[] buttons)
        {
            playerTurn = 1;
            foreach (Button b in buttons)
            {
                b.Text = "";
                b.BackgroundColor = Color.Coral;
            }
        }
    }
}
